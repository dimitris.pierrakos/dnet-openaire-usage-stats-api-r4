package eu.dnetlib.usagestats.services;

import eu.dnetlib.usagestats.portal.CountryRepositories;
import eu.dnetlib.usagestats.portal.CountryUsageStats;
import eu.dnetlib.usagestats.portal.CountryUsageStatsAll;
import eu.dnetlib.usagestats.portal.MonthlyUsageStats;
import eu.dnetlib.usagestats.portal.TotalStats;
import eu.dnetlib.usagestats.portal.TotalStatsReposViewsDownloads;
import eu.dnetlib.usagestats.portal.UsageStats;
import eu.dnetlib.usagestats.repositories.UsageStatsRepository;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class UsageStatsServiceImpl implements UsageStatsService {
    private final UsageStatsRepository usageStatsRepository;
    private final Logger log = Logger.getLogger(this.getClass());
    @Value("${prod.statsdb}")
    private String statsDB;
    @Value("${prod.usagestatsImpalaDB}")
    private String usagestatsImpalaDB;

    public UsageStatsServiceImpl(UsageStatsRepository usageStatsRepository) {
        this.usageStatsRepository = usageStatsRepository;
    }

    public UsageStats getDatasourceClicks(String id) {
        String query = "SELECT 'views', sum(s.count), sum(s.openaire) FROM " + this.usagestatsImpalaDB + ".views_stats s where s.repository_id=? UNION ALL SELECT 'downloads', sum(s.count), sum(s.openaire) FROM " + this.usagestatsImpalaDB + ".downloads_stats s where s.repository_id=? UNION ALL SELECT 'pageviews', sum(s.count), 0 FROM " + this.usagestatsImpalaDB + ".pageviews_stats s, " + this.statsDB + ".result_datasources rd where rd.id=s.result_id and rd.datasource=? ";
        List<String> values = new ArrayList();
        values.add(id);
        values.add(id);
        values.add(id);
        return this.usageStatsRepository.executeUsageStats(query, values, "datasource");
    }

    public UsageStats getProjectClicks(String projectId) {
        String query = "SELECT 'views', sum(s.count), sum(s.openaire) FROM " + this.usagestatsImpalaDB + ".views_stats s, " + this.statsDB + ".project_results pr where pr.result=s.result_id and pr.id=? UNION ALL SELECT 'downloads', sum(s.count), sum(s.openaire) FROM " + this.usagestatsImpalaDB + ".downloads_stats s, " + this.statsDB + ".project_results pr where pr.result=s.result_id and pr.id=? UNION ALL SELECT 'pageviews', sum(s.count), 0 FROM " + this.usagestatsImpalaDB + ".pageviews_stats s, " + this.statsDB + ".project_results pr where pr.result=s.result_id and pr.id=?;";
        List<String> values = new ArrayList();
        values.add(projectId);
        values.add(projectId);
        values.add(projectId);
        return this.usageStatsRepository.executeUsageStats(query, values, "project");
    }

    public UsageStats getResultClicks(String id) {
        String query = "SELECT 'views', s.repository_id, CASE WHEN s.source='OpenAIRE' THEN d.name ELSE concat(d.name,' - ',s.source) END, sum(count), sum(openaire) FROM (select distinct * from " + this.usagestatsImpalaDB + ".views_stats) s, " + this.statsDB + ".datasource d WHERE s.repository_id=d.id AND s.result_id=? GROUP BY s.source, s.repository_id, d.name UNION ALL SELECT 'downloads', s.repository_id, CASE WHEN s.source='OpenAIRE' THEN d.name ELSE concat(d.name,' - ',s.source) END, sum(count), sum(s.openaire) FROM (select distinct * from " + this.usagestatsImpalaDB + ".downloads_stats) s, " + this.statsDB + ".datasource d WHERE s.repository_id=d.id AND s.result_id=? GROUP BY s.source, s.repository_id, d.name UNION ALL SELECT 'pageviews', 'OpenAIRE id', 'OpenAIRE', sum(count), 0 FROM " + this.usagestatsImpalaDB + ".pageviews_stats s WHERE result_id=?;";
        List<String> values = new ArrayList();
        values.add(id);
        values.add(id);
        values.add(id);
        return this.usageStatsRepository.executeUsageStats(query, values, "result");
    }

    public TotalStats getTotalStats() {
        return this.usageStatsRepository.executeTotalStats();
    }

    public List<MonthlyUsageStats> getMonthlyUsageStats() {
        String query = "select `date`, sum(downloads) as downloads, sum(views) as views from " + this.usagestatsImpalaDB + ".usage_stats group by `date` order by `date` asc";
        return this.usageStatsRepository.executeMontlyUsageStats(query);
    }

    public List<MonthlyUsageStats> getMonthlyUsageStatsForRepo(String id) {
        String query = "select `date`, sum(downloads) as downloads, sum(views) as views from " + this.usagestatsImpalaDB + ".usage_stats where repository_id=? group by `date` order by `date` asc";
        return this.usageStatsRepository.executeMontlyUsageStatsForRepo(query, id);
    }

    public CountryUsageStatsAll getCountryUsageStatsAll() {
        String query = "SELECT c.name, count(distinct repository_id) as total_repos, sum(views) as views, sum(downloads) as downloads FROM " + this.statsDB + ".organization t, " + this.statsDB + ".organization_datasources o, " + this.statsDB + ".country c, " + this.usagestatsImpalaDB + ".usage_stats WHERE o.id=t.id AND c.code=t.country AND o.datasource=repository_id GROUP BY c.name";
        return this.usageStatsRepository.executeCountryUsageStats(query);
    }

    public CountryUsageStats getCountryUsageStats(String country) {
        String query = "SELECT count(distinct repository_id) as total_repos, sum(views) as views, sum(downloads) as downloads from " + this.statsDB + ".organization t, " + this.statsDB + ".organization_datasources o, " + this.statsDB + ".country c, " + this.usagestatsImpalaDB + ".usage_stats where o.id=t.id and c.code=t.country and o.datasource=repository_id and c.name='" + country + "'";
        this.log.info("Country Usage Stats query " + query);
        return this.usageStatsRepository.executeCountryUsageStats(query, country);
    }

    public List<CountryRepositories> getCountryRepositories() {
        String query = "SELECT c.name, d.name FROM " + this.statsDB + ".datasource d, " + this.statsDB + ".organization t, " + this.statsDB + ".organization_datasources o, " + this.statsDB + ".country c, " + this.usagestatsImpalaDB + ".usage_stats WHERE o.id=t.id AND c.code=t.country AND o.datasource=repository_id and repository_id=d.id GROUP BY d.name, c.name ORDER BY c.name";
        return this.usageStatsRepository.executeCountryRepositories(query);
    }

    public TotalStatsReposViewsDownloads getTotalStatsReposViewsDownloads() {
        String query = "SELECT COUNT(DISTINCT repository_id) as repositories, sum(views) as views, sum(downloads) as downloads from " + this.usagestatsImpalaDB + ".usage_stats";
        this.log.info("Total Repositories-Views-Downlaods " + query);
        return this.usageStatsRepository.executeTotalStatsReposViewsDownloads(query);
    }
}