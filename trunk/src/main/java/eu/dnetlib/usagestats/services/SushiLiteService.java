package eu.dnetlib.usagestats.services;

import eu.dnetlib.usagestats.sushilite.domain.ReportResponseWrapper;

public interface SushiLiteService {
    ReportResponseWrapper buildReport(String var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8, String var9, String var10, String var11);

    String displayReport(String var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8, String var9, String var10, String var11, String var12);
}
