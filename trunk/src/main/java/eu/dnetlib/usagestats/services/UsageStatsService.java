package eu.dnetlib.usagestats.services;

import eu.dnetlib.usagestats.portal.CountryRepositories;
import eu.dnetlib.usagestats.portal.CountryUsageStats;
import eu.dnetlib.usagestats.portal.CountryUsageStatsAll;
import eu.dnetlib.usagestats.portal.MonthlyUsageStats;
import eu.dnetlib.usagestats.portal.TotalStats;
import eu.dnetlib.usagestats.portal.TotalStatsReposViewsDownloads;
import eu.dnetlib.usagestats.portal.UsageStats;
import java.util.List;

public interface UsageStatsService {
    UsageStats getDatasourceClicks(String var1);

    UsageStats getProjectClicks(String var1);

    UsageStats getResultClicks(String var1);

    TotalStats getTotalStats();

    List<MonthlyUsageStats> getMonthlyUsageStats();

    List<MonthlyUsageStats> getMonthlyUsageStatsForRepo(String var1);

    CountryUsageStatsAll getCountryUsageStatsAll();

    CountryUsageStats getCountryUsageStats(String var1);

    List<CountryRepositories> getCountryRepositories();

    TotalStatsReposViewsDownloads getTotalStatsReposViewsDownloads();
}