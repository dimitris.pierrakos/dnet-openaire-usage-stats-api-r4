package eu.dnetlib.usagestats.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.dnetlib.usagestats.repositories.UsageStatsRepository;
import eu.dnetlib.usagestats.sushilite.domain.ReportException;
import eu.dnetlib.usagestats.sushilite.domain.ReportItem;
import eu.dnetlib.usagestats.sushilite.domain.ReportResponse;
import eu.dnetlib.usagestats.sushilite.domain.ReportResponseWrapper;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class SushiLiteServiceImpl implements SushiLiteService {
    private final UsageStatsRepository usageStatsRepository;
    private final Logger log = Logger.getLogger(this.getClass());

    public SushiLiteServiceImpl(UsageStatsRepository usageStatsRepository) {
        this.usageStatsRepository = usageStatsRepository;
    }

    public ReportResponseWrapper buildReport(String reportName, String release, String requestorId, String beginDate, String endDate, String repositoryIdentifier, String itemIdentifier, String itemDataType, String hasDoi, String granularity, String callback) {
        List<ReportItem> reportItems = new ArrayList();
        List<ReportException> reportExceptions = new ArrayList();
        if (!granularity.equalsIgnoreCase("totals") && !granularity.equalsIgnoreCase("monthly")) {
            reportExceptions.add(new ReportException("3062", "Warning", "Invalid ReportAttribute Value", "Granularity: '" + granularity + "' unknown. Defaulting to Monthly"));
            granularity = "Monthly";
        }

        Date beginDateParsed;
        Calendar temp;
        if (!beginDate.equals("")) {
            beginDateParsed = this.tryParse(beginDate);
            if (beginDateParsed != null && (granularity.toLowerCase().equals("monthly") || beginDate.length() == 7)) {
                temp = Calendar.getInstance();
                temp.setTime(beginDateParsed);
                temp.set(5, temp.getActualMinimum(5));
                beginDateParsed = temp.getTime();
            }
        } else {
            temp = Calendar.getInstance();
            temp.add(2, -1);
            temp.set(5, temp.getActualMinimum(5));
            beginDateParsed = temp.getTime();
            reportExceptions.add(new ReportException("3021", "Warning", "Unspecified Date Arguments", "Begin Date set to default: " + (new SimpleDateFormat("yyyy-MM-dd")).format(beginDateParsed)));
        }

//        Calendar temp;
        Date endDateParsed;
        if (!endDate.equals("")) {
            endDateParsed = this.tryParse(endDate);
            if (endDateParsed != null && (granularity.toLowerCase().equals("monthly") || endDate.length() == 7)) {
                temp = Calendar.getInstance();
                temp.setTime(endDateParsed);
                temp.set(5, temp.getActualMaximum(5));
                endDateParsed = temp.getTime();
            }
        } else {
            temp = Calendar.getInstance();
            temp.add(2, -1);
            temp.set(5, temp.getActualMaximum(5));
            endDateParsed = temp.getTime();
            reportExceptions.add(new ReportException("3021", "Warning", "Unspecified Date Arguments", "End Date set to default: " + (new SimpleDateFormat("yyyy-MM-dd")).format(endDateParsed)));
        }

        if (beginDateParsed == null) {
            reportExceptions.add(new ReportException("3020", "Error", "Invalid Date Arguments", "Begin Date: " + beginDate + " is not a valid date"));
        }

        if (endDateParsed == null) {
            reportExceptions.add(new ReportException("3020", "Error", "Invalid Date Arguments", "End Date: " + endDate + " is not a valid date"));
        }

        if (beginDateParsed != null && endDateParsed != null && !beginDateParsed.before(endDateParsed)) {
            reportExceptions.add(new ReportException("3020", "Error", "Invalid Date Arguments", "BeginDate '" + (new SimpleDateFormat("yyyy-MM-dd")).format(beginDateParsed) + "' is greater than EndDate '" + (new SimpleDateFormat("yyyy-MM-dd")).format(endDateParsed) + "'"));
        }

        String repoid = "";
        if (!repositoryIdentifier.equals("")) {
            repoid = this.usageStatsRepository.executeRepoId(repositoryIdentifier, reportName.toLowerCase());
            if (repoid.equals("-1")) {
                reportExceptions.add(new ReportException("3060", "Error", "Invalid Filter Value", "RepositoryIdentifier: " + repositoryIdentifier + " is not valid"));
            }
        }

        String itemid = "";
        if (!itemIdentifier.equals("")) {
            String[] split = itemIdentifier.split(":");
            String var19 = split[0].toLowerCase();
            byte var20 = -1;
            switch(var19.hashCode()) {
            case -504258139:
                if (var19.equals("openaire")) {
                    var20 = 2;
                }
                break;
            case 99646:
                if (var19.equals("doi")) {
                    var20 = 1;
                }
                break;
            case 110026:
                if (var19.equals("oid")) {
                    var20 = 0;
                }
            }

            switch(var20) {
            case 0:
                itemid = itemIdentifier;
                break;
            case 1:
                itemid = itemIdentifier;
                break;
            case 2:
                itemid = itemIdentifier;
                break;
            default:
                reportExceptions.add(new ReportException("3060", "Error", "Invalid Filter Value", "ItemIdentifier: " + itemIdentifier + " is not valid"));
                itemid = "-1";
            }
        }

        if (itemid.equals("") && repoid.equals("") && !reportName.equalsIgnoreCase("rr1") && !reportName.equalsIgnoreCase("jr1")) {
            reportExceptions.add(new ReportException("3070", "Error", "Required Filter Missing", "ItemIdentifier or RepositoryIdentifier must be supplied"));
        }

        if (reportName.equalsIgnoreCase("ar1")) {
            if (!itemid.equals("-1") && !repoid.equals("-1") && beginDateParsed != null && endDateParsed != null && beginDateParsed.before(endDateParsed)) {
                if (!itemid.equals("")) {
                    if (!itemDataType.equalsIgnoreCase("") && !itemDataType.equalsIgnoreCase("article")) {
                        reportExceptions.add(new ReportException("3030", "Error", "No Usage Available for Requested Dates", "Service did not find any data"));
                    } else {
                        this.usageStatsRepository.executeItem(reportItems, itemIdentifier, repoid, "Article", beginDateParsed, endDateParsed, granularity);
                        if (reportItems.isEmpty()) {
                            reportExceptions.add(new ReportException("3030", "Error", "No Usage Available for Requested Dates", "Service did not find any data"));
                        }
                    }
                } else if (!repoid.equals("")) {
                    this.usageStatsRepository.executeBatchItems(reportItems, repoid, "Article", beginDateParsed, endDateParsed, granularity);
                    if (reportItems.isEmpty()) {
                        reportExceptions.add(new ReportException("3030", "Error", "No Usage Available for Requested Dates", "Service did not find any data"));
                    }
                }
            }
        } else if (reportName.equalsIgnoreCase("br1")) {
            if (!itemid.equals("-1") && !repoid.equals("-1") && beginDateParsed != null && endDateParsed != null && beginDateParsed.before(endDateParsed)) {
                if (!itemid.equals("")) {
                    if (!itemDataType.equalsIgnoreCase("") && !itemDataType.equalsIgnoreCase("book")) {
                        reportExceptions.add(new ReportException("3030", "Error", "No Usage Available for Requested Dates", "Service did not find any data"));
                    } else {
                        this.usageStatsRepository.executeItem(reportItems, itemIdentifier, repoid, "Book", beginDateParsed, endDateParsed, granularity);
                        if (reportItems.isEmpty()) {
                            reportExceptions.add(new ReportException("3030", "Error", "No Usage Available for Requested Dates", "Service did not find any data"));
                        }
                    }
                } else if (!repoid.equals("")) {
                    this.usageStatsRepository.executeBatchItems(reportItems, repoid, "Book", beginDateParsed, endDateParsed, granularity);
                    if (reportItems.isEmpty()) {
                        reportExceptions.add(new ReportException("3030", "Error", "No Usage Available for Requested Dates", "Service did not find any data"));
                    }
                }
            }
        } else if (reportName.equalsIgnoreCase("br2")) {
            if (!itemid.equals("-1") && !repoid.equals("-1") && beginDateParsed != null && endDateParsed != null && beginDateParsed.before(endDateParsed)) {
                if (!itemid.equals("")) {
                    if (!itemDataType.equalsIgnoreCase("") && !itemDataType.equalsIgnoreCase("part of book or chapter of book")) {
                        reportExceptions.add(new ReportException("3030", "Error", "No Usage Available for Requested Dates", "Service did not find any data"));
                    } else {
                        this.usageStatsRepository.executeItem(reportItems, itemIdentifier, repoid, "Part of book or chapter of book", beginDateParsed, endDateParsed, granularity);
                        if (reportItems.isEmpty()) {
                            reportExceptions.add(new ReportException("3030", "Error", "No Usage Available for Requested Dates", "Service did not find any data"));
                        }
                    }
                } else if (!repoid.equals("")) {
                    this.usageStatsRepository.executeBatchItems(reportItems, repoid, "Part of book or chapter of book", beginDateParsed, endDateParsed, granularity);
                    if (reportItems.isEmpty()) {
                        reportExceptions.add(new ReportException("3030", "Error", "No Usage Available for Requested Dates", "Service did not find any data"));
                    }
                }
            }
        } else if (reportName.equalsIgnoreCase("ir1")) {
            if (!itemid.equals("-1") && !repoid.equals("-1") && beginDateParsed != null && endDateParsed != null && beginDateParsed.before(endDateParsed)) {
                if (!itemid.equals("")) {
                    this.usageStatsRepository.executeItem(reportItems, itemIdentifier, repoid, itemDataType, beginDateParsed, endDateParsed, granularity);
                } else if (!repoid.equals("")) {
                    this.usageStatsRepository.executeBatchItems(reportItems, repoid, itemDataType, beginDateParsed, endDateParsed, granularity);
                }

                if (reportItems.isEmpty()) {
                    reportExceptions.add(new ReportException("3030", "Error", "No Usage Available for Requested Dates", "Service did not find any data"));
                }
            }
        } else if (reportName.equalsIgnoreCase("rr1")) {
            if (!repoid.equals("-1") && beginDateParsed != null && endDateParsed != null && beginDateParsed.before(endDateParsed)) {
                this.usageStatsRepository.executeRepo(reportItems, repoid, itemDataType, beginDateParsed, endDateParsed, granularity);
            }

            if (reportItems.isEmpty()) {
                reportExceptions.add(new ReportException("3030", "Error", "No Usage Available for Requested Dates", "Service did not find any data"));
            }
        } else if (reportName.equalsIgnoreCase("jr1")) {
            if (!repoid.equals("-1") && beginDateParsed != null && endDateParsed != null && beginDateParsed.before(endDateParsed)) {
                this.usageStatsRepository.executeJournal(reportItems, repoid, itemDataType, beginDateParsed, endDateParsed, granularity);
            }

            if (reportItems.isEmpty()) {
                reportExceptions.add(new ReportException("3030", "Error", "No Usage Available for Requested Dates", "Service did not find any data"));
            }
        } else if (reportName.equals("")) {
            reportExceptions.add(new ReportException("3050", "Error", "Report argument is missing", "You must supply a Report argument"));
        } else {
            reportExceptions.add(new ReportException("3000", "Error", "Report " + reportName + " not supported", "Supported reports: AR1, IR1, RR1, BR1, BR2"));
        }

        ReportResponse reportResponse = new ReportResponse(reportName, release, requestorId, beginDate, endDate, repositoryIdentifier, itemIdentifier, itemDataType, hasDoi, granularity, callback, reportItems, reportExceptions);
        return new ReportResponseWrapper(reportResponse);
    }

    public String displayReport(String reportName, String release, String requestorId, String beginDate, String endDate, String repositoryIdentifier, String itemIdentifier, String itemDataType, String hasDoi, String granularity, String callback, String pretty) {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            return pretty.equalsIgnoreCase("pretty") ? "<pre>" + objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(this.buildReport(reportName, release, requestorId, beginDate, endDate, repositoryIdentifier, itemIdentifier, itemDataType, hasDoi, granularity, callback)).replaceAll("/", "\\\\/") + "</pre>" : objectMapper.writeValueAsString(this.buildReport(reportName, release, requestorId, beginDate, endDate, repositoryIdentifier, itemIdentifier, itemDataType, hasDoi, granularity, callback)).replaceAll("/", "\\\\/");
        } catch (Exception var15) {
            var15.printStackTrace();
            return null;
        }
    }

    private Date tryParse(String dateString) {
        try {
            if (dateString.length() == 7) {
                return (new SimpleDateFormat("yyyy-MM")).parse(dateString);
            }

            if (dateString.length() == 10) {
                return (new SimpleDateFormat("yyyy-MM-dd")).parse(dateString);
            }
        } catch (Exception var3) {
            this.log.error("ParseError: ", var3);
        }

        return null;
    }
}