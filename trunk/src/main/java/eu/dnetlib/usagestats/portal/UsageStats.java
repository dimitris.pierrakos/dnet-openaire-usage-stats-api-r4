package eu.dnetlib.usagestats.portal;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UsageStats implements Serializable {
    private static final long serialVersionUID = 1L;
    private final List<RepositoryStats> downloads = new ArrayList();
    private final List<RepositoryStats> views = new ArrayList();
    private String total_downloads = "0";
    private String total_views = "0";
    private String pageviews = "0";
    private String total_openaire_views = "0";
    private String total_openaire_downloads = "0";

    public UsageStats() {
    }

    @JsonProperty("downloads")
    public List<RepositoryStats> getDownloads() {
        return this.downloads;
    }

    @JsonProperty("views")
    public List<RepositoryStats> getViews() {
        return this.views;
    }

    public void addViews(RepositoryStats view) {
        this.views.add(view);
    }

    public void addDownloads(RepositoryStats download) {
        this.downloads.add(download);
    }

    @JsonProperty("total_downloads")
    public String getTotal_downloads() {
        return this.total_downloads;
    }

    public void setTotal_downloads(String total_downloads) {
        this.total_downloads = total_downloads;
    }

    @JsonProperty("total_views")
    public String getTotal_views() {
        return this.total_views;
    }

    public void setTotal_views(String total_views) {
        this.total_views = total_views;
    }

    @JsonProperty("pageviews")
    public String getPageViews() {
        return this.pageviews;
    }

    public void setPageViews(String pageviews) {
        this.pageviews = pageviews;
    }

    @JsonProperty("total_openaire_views")
    public String getTotal_openaire_views() {
        return this.total_openaire_views;
    }

    public void setTotal_openaire_views(String total_openaire_views) {
        this.total_openaire_views = total_openaire_views;
    }

    @JsonProperty("total_openaire_downloads")
    public String getTotal_openaire_downloads() {
        return this.total_openaire_downloads;
    }

    public void setTotal_openaire_downloads(String total_openaire_downloads) {
        this.total_openaire_downloads = total_openaire_downloads;
    }
}