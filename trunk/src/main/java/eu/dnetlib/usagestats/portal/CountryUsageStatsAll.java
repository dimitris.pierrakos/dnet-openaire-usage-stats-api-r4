package eu.dnetlib.usagestats.portal;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CountryUsageStatsAll implements Serializable {
    private static final long serialVersionUID = 1L;
    private String country_all = "all";
    private String downloads_all = "0";
    private String views_all = "0";
    private List<CountryUsageStats> countryUsageStats = new ArrayList();

    public CountryUsageStatsAll() {
    }

    @JsonProperty("country")
    public String getCountry() {
        return this.country_all;
    }

    @JsonProperty("downloads_all")
    public String getDownloads() {
        return this.downloads_all;
    }

    @JsonProperty("views_all")
    public String getViews() {
        return this.views_all;
    }

    @JsonProperty("CountriesList")
    public List<CountryUsageStats> getCountryUsageStats() {
        return this.countryUsageStats;
    }

    public void addViewsAll(String views_all) {
        this.views_all = views_all;
    }

    public void addDownloadsAll(String downloads_all) {
        this.downloads_all = downloads_all;
    }

    public void addCountryUsageStats(List<CountryUsageStats> countryUsageStats) {
        this.countryUsageStats = countryUsageStats;
    }
}