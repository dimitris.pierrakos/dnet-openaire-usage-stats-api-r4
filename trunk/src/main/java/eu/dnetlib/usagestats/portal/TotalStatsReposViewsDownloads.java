package eu.dnetlib.usagestats.portal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TotalStatsReposViewsDownloads {
    private String repositories;
    private String downloads;
    private String views;

    public TotalStatsReposViewsDownloads() {
    }

    @JsonProperty("repositories")
    public String getRepositories() {
        return this.repositories;
    }

    public void addRepositories(String repositories) {
        this.repositories = repositories;
    }

    @JsonProperty("total_views")
    public String getViews() {
        return this.views;
    }

    public void addViews(String views) {
        this.views = views;
    }

    @JsonProperty("total_downloads")
    public String getDownloads() {
        return this.downloads;
    }

    public void addDownloads(String downloads) {
        this.downloads = downloads;
    }
}