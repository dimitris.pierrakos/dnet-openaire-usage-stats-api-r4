package eu.dnetlib.usagestats.portal;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

public class CountryRepositories implements Serializable {
    private static final long serialVersionUID = 1L;
    private String country = "";
    private String repository = "";

    public CountryRepositories() {
    }

    @JsonProperty("country")
    public String getCountry() {
        return this.country;
    }

    @JsonProperty("repository")
    public String getRepository() {
        return this.repository;
    }

    public void addCountry(String country) {
        this.country = country;
    }

    public void addRepository(String repository) {
        this.repository = repository;
    }
}
