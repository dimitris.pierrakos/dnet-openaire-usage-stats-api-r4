package eu.dnetlib.usagestats.portal;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

public class CountryUsageStats implements Serializable {
    private static final long serialVersionUID = 1L;
    private String country = "";
    private String total_repos = "";
    private String downloads = "";
    private String views = "";

    public CountryUsageStats() {
    }

    @JsonProperty("country")
    public String getCountry() {
        return this.country;
    }

    @JsonProperty("total_repos")
    public String getTotalRepos() {
        return this.total_repos;
    }

    @JsonProperty("downloads")
    public String getDownloads() {
        return this.downloads;
    }

    @JsonProperty("views")
    public String getViews() {
        return this.views;
    }

    public void addCountry(String country) {
        this.country = country;
    }

    public void addTotalRepos(String total_repos) {
        this.total_repos = total_repos;
    }

    public void addViews(String views) {
        this.views = views;
    }

    public void addDownloads(String downloads) {
        this.downloads = downloads;
    }
}