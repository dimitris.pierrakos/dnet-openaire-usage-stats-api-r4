package eu.dnetlib.usagestats.portal;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

public class RepositoryStats implements Serializable {
    private static final long serialVersionUID = 1L;
    private String datasource_name = "";
    private String datasource_id = "";
    private String value = "";
    private String openaire = "";

    public RepositoryStats() {
    }

    public RepositoryStats(String datasource_name, String datasource_id, String value, String openaire) {
        this.datasource_name = datasource_name;
        this.datasource_id = datasource_id;
        this.value = value;
        this.openaire = openaire;
    }

    @JsonProperty("datasource_name")
    public String getDatasource_name() {
        return this.datasource_name;
    }

    @JsonProperty("datasource_id")
    public String getDatasource_id() {
        return this.datasource_id;
    }

    @JsonProperty("value")
    public String getValue() {
        return this.value;
    }

    @JsonProperty("openaire")
    public String getOpenaire() {
        return this.openaire;
    }
}