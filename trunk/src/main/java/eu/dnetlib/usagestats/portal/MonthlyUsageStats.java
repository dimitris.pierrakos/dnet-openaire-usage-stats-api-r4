package eu.dnetlib.usagestats.portal;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

public class MonthlyUsageStats implements Serializable {
    private static final long serialVersionUID = 1L;
    private String date = "0";
    private String downloads = "0";
    private String views = "0";

    public MonthlyUsageStats() {
    }

    @JsonProperty("date")
    public String getDate() {
        return this.date;
    }

    @JsonProperty("downloads")
    public String getDownloads() {
        return this.downloads;
    }

    @JsonProperty("views")
    public String getViews() {
        return this.views;
    }

    public void addDate(String date) {
        this.date = date;
    }

    public void addViews(String views) {
        this.views = views;
    }

    public void addDownloads(String downloads) {
        this.downloads = downloads;
    }
}
