package eu.dnetlib.usagestats.portal;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class YearlyStats {
    private int year;
    private int repositories;
    private int items;
    private int downloads;
    private int views;
    private List<MonthlyStats> monthlyStats;

    @JsonProperty("monthly_stats")
    public List<MonthlyStats> getMonthlyStats() {
        return this.monthlyStats;
    }

    public void setMonthlyStats(List<MonthlyStats> monthlyStats) {
        this.monthlyStats = monthlyStats;
    }

    public YearlyStats() {
    }

    public int getYear() {
        return this.year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getRepositories() {
        return this.repositories;
    }

    public void setRepositories(int repositories) {
        this.repositories = repositories;
    }

    public int getItems() {
        return this.items;
    }

    public void setItems(int items) {
        this.items = items;
    }

    public int getDownloads() {
        return this.downloads;
    }

    public void setDownloads(int downloads) {
        this.downloads = downloads;
    }

    public int getViews() {
        return this.views;
    }

    public void setViews(int views) {
        this.views = views;
    }
}