package eu.dnetlib.usagestats.portal;

public class MonthlyStats {
    private int month;
    private int repositories;
    private int items;
    private int downloads;
    private int views;

    public MonthlyStats() {
    }

    public int getMonth() {
        return this.month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getRepositories() {
        return this.repositories;
    }

    public void setRepositories(int repositories) {
        this.repositories = repositories;
    }

    public int getItems() {
        return this.items;
    }

    public void setItems(int items) {
        this.items = items;
    }

    public int getDownloads() {
        return this.downloads;
    }

    public void setDownloads(int downloads) {
        this.downloads = downloads;
    }

    public int getViews() {
        return this.views;
    }

    public void setViews(int views) {
        this.views = views;
    }
}