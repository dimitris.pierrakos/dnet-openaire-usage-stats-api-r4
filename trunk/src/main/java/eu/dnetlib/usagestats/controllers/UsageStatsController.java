package eu.dnetlib.usagestats.controllers;

import eu.dnetlib.usagestats.portal.CountryRepositories;
import eu.dnetlib.usagestats.portal.CountryUsageStats;
import eu.dnetlib.usagestats.portal.CountryUsageStatsAll;
import eu.dnetlib.usagestats.portal.MonthlyUsageStats;
import eu.dnetlib.usagestats.portal.TotalStats;
import eu.dnetlib.usagestats.portal.TotalStatsReposViewsDownloads;
import eu.dnetlib.usagestats.portal.UsageStats;
import eu.dnetlib.usagestats.services.UsageStatsService;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(
    methods = {RequestMethod.GET},
    origins = {"*"}
)
public class UsageStatsController {
    private final UsageStatsService usageStatsService;
    private final Logger log = Logger.getLogger(this.getClass());

    public UsageStatsController(UsageStatsService usageStatsService) {
        this.usageStatsService = usageStatsService;
    }

    @RequestMapping({"/datasources/{datasourceId}/clicks"})
    public UsageStats getDatasourceClicks(@PathVariable("datasourceId") String datasourceId) {
        this.log.info("stats request for datasource: " + datasourceId);
        return this.usageStatsService.getDatasourceClicks(datasourceId);
    }

    @RequestMapping({"/projects/{projectId}/clicks"})
    public UsageStats getProjectClicks(@PathVariable("projectId") String projectId) {
        this.log.info("stats request for project: " + projectId);
        return this.usageStatsService.getProjectClicks(projectId);
    }

    @RequestMapping({"/monthlyusagestats"})
    public List<MonthlyUsageStats> getMonthlyUsageStats() {
        this.log.info("stats request for months");
        return this.usageStatsService.getMonthlyUsageStats();
    }

    @RequestMapping({"/countryusagestats"})
    public CountryUsageStatsAll getCountryUsageStatsAll() {
        this.log.info("stats request for countries");
        return this.usageStatsService.getCountryUsageStatsAll();
    }

    @RequestMapping({"/countryusagestats/{country}"})
    public CountryUsageStats getCountryUsageStats(@PathVariable("country") String country) {
        this.log.info("stats request for country " + country);
        return this.usageStatsService.getCountryUsageStats(country);
    }

    @RequestMapping({"/countryrepositories"})
    public List<CountryRepositories> getCountryRepositories() {
        this.log.info("stats request for countries/repos");
        return this.usageStatsService.getCountryRepositories();
    }

    @RequestMapping({"/totals"})
    public TotalStats getTotalStats() {
        this.log.info("total stats request");
        return this.usageStatsService.getTotalStats();
    }

    @RequestMapping({"/monthlyusagestats/{datasourceId}"})
    public List<MonthlyUsageStats> getMonthlyUsageStatsForRepo(@PathVariable("datasourceId") String datasourceId) {
        this.log.info("stats request for datasource: " + datasourceId);
        return this.usageStatsService.getMonthlyUsageStatsForRepo(datasourceId);
    }

    @RequestMapping({"/results/{resultId}/clicks"})
    public UsageStats getResultClicks(@PathVariable("resultId") String resultId) {
        this.log.info("stats request for result: " + resultId);
        return this.usageStatsService.getResultClicks(resultId);
    }

    @RequestMapping({"/allmetrics"})
    public TotalStatsReposViewsDownloads getTotalStatsReposViewsDownloads() {
        this.log.info("total stats request");
        return this.usageStatsService.getTotalStatsReposViewsDownloads();
    }
}