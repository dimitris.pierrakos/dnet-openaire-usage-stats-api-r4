package eu.dnetlib.usagestats.controllers;

import eu.dnetlib.usagestats.services.SushiLiteService;
import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;

@RestController
class SushiLiteController {
    private final Logger log = Logger.getLogger(this.getClass());
    private final SushiLiteService sushiLiteService;

    public SushiLiteController(SushiLiteService sushiLiteService) {
        this.sushiLiteService = sushiLiteService;
    }

    @RequestMapping(
        value = {"/sushilite/GetReport/"},
        method = {RequestMethod.GET}
    )
    public String getReport(@RequestParam(value = "Report",defaultValue = "") String reportP,
                            @RequestParam(value = "Release",defaultValue = "4") String release,
                            @RequestParam(value = "RequestorID",defaultValue = "anonymous") String requestorId,
                            @RequestParam(value = "BeginDate",defaultValue = "") String beginDate,
                            @RequestParam(value = "EndDate",defaultValue = "") String endDate,
                            @RequestParam(value = "RepositoryIdentifier",defaultValue = "") String repositoryIdentifier,
                            @RequestParam(value = "ItemIdentifier",defaultValue = "") String itemIdentifier,
                            @RequestParam(value = "ItemDataType",defaultValue = "") String itemDataType,
                            @RequestParam(value = "hasDOI",defaultValue = "") String hasDoi,
                            @RequestParam(value = "Granularity",defaultValue = "Monthly") String granularity,
                            @RequestParam(value = "Callback",defaultValue = "") String callback,
                            @RequestParam(value = "Pretty",defaultValue = "") String pretty) {
        this.log.info("Sushi Report request: " + reportP + " from " + requestorId);
        this.log.info("repository identifier: " + repositoryIdentifier + " - item identifier: " + itemIdentifier);
        return this.sushiLiteService.displayReport(reportP, release, requestorId, beginDate, endDate, repositoryIdentifier, itemIdentifier, itemDataType, hasDoi, granularity, callback, pretty);
    }

    @RequestMapping(value = {"/sushilite/DownloadReport/"},
            method = RequestMethod.GET)
    public HttpEntity<byte[]> downloadExcelReport(@RequestParam(value = "Report",defaultValue = "") String reportP,
                                                  @RequestParam(value = "Release",defaultValue = "4") String release,
                                                  @RequestParam(value = "RequestorID",defaultValue = "anonymous") String requestorId,
                                                  @RequestParam(value = "BeginDate",defaultValue = "") String beginDate,
                                                  @RequestParam(value = "EndDate",defaultValue = "") String endDate,
                                                  @RequestParam(value = "RepositoryIdentifier",defaultValue = "") String repositoryIdentifier,
                                                  @RequestParam(value = "ItemIdentifier",defaultValue = "") String itemIdentifier,
                                                  @RequestParam(value = "ItemDataType",defaultValue = "") String itemDataType,
                                                  @RequestParam(value = "hasDOI",defaultValue = "") String hasDoi,
                                                  @RequestParam(value = "Granularity",defaultValue = "Monthly") String granularity,
                                                  @RequestParam(value = "Callback",defaultValue = "") String callback,
                                                  @RequestParam(value = "Pretty",defaultValue = "") String pretty) {

        this.log.info("Sushi Report request: " + reportP + " from " + requestorId);
        this.log.info("repository identifier: " + repositoryIdentifier + " - item identifier: " + itemIdentifier);

        byte[] jsonContent = this.sushiLiteService.displayReport(reportP, release, requestorId, beginDate, endDate, repositoryIdentifier, itemIdentifier, itemDataType, hasDoi, granularity, callback, "").getBytes(StandardCharsets.UTF_8);
        HttpHeaders header = new HttpHeaders();
        header.setContentType(new MediaType("application", "json"));
        header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename="+reportP+".json");
        header.setContentLength(jsonContent.length);

        return new HttpEntity<byte[]>(jsonContent, header);
    }
}