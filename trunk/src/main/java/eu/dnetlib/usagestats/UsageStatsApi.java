package eu.dnetlib.usagestats;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.retry.annotation.EnableRetry;

@PropertySources({@PropertySource({"classpath:usageStatsAPI.properties"})})
@EnableRetry
@SpringBootApplication
public class UsageStatsApi extends SpringBootServletInitializer {
    public UsageStatsApi() {
    }

    public static void main(String[] args) {
        SpringApplication.run(UsageStatsApi.class, args);
    }
}