package eu.dnetlib.usagestats.config;

import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class DataSourceConfiguration {
    private final Logger log = Logger.getLogger(this.getClass());
    @Value("${usagestats.driverClassName}")
    private String driverClassName;
    @Value("${usagestats.url}")
    private String dbURL;

    public DataSourceConfiguration() {
    }

    @ConfigurationProperties(
        prefix = "usagestats"
    )
    @Bean
    @Primary
    public DataSource getDataSource() {
        PoolProperties poolProperties = new PoolProperties();
        poolProperties.setUrl(this.dbURL);
        poolProperties.setDriverClassName(this.driverClassName);
        this.log.info("dbURL " + this.dbURL);
        this.log.info("driverClassName " + this.driverClassName);
        poolProperties.setTestOnBorrow(true);
        poolProperties.setValidationQuery("SELECT 1");
        poolProperties.setValidationInterval(0L);
        DataSource ds = new org.apache.tomcat.jdbc.pool.DataSource(poolProperties);
        return ds;
    }
}