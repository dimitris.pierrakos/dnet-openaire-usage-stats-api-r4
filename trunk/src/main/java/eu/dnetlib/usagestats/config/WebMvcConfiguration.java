package eu.dnetlib.usagestats.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebMvcConfiguration extends WebMvcConfigurerAdapter {
    public WebMvcConfiguration() {
    }

    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/sushilite").setViewName("redirect:/sushilite/");
        registry.addViewController("/sushilite/").setViewName("forward:/sushilite/index.html");
        registry.addViewController("/sushilite/AR1").setViewName("redirect:/sushilite/AR1/");
        registry.addViewController("/sushilite/AR1/").setViewName("forward:/sushilite/AR1/index.html");
        registry.addViewController("/sushilite/IR1").setViewName("redirect:/sushilite/IR1/");
        registry.addViewController("/sushilite/IR1/").setViewName("forward:/sushilite/IR1/index.html");
        registry.addViewController("/sushilite/RR1").setViewName("redirect:/sushilite/RR1/");
        registry.addViewController("/sushilite/RR1/").setViewName("forward:/sushilite/RR1/index.html");
        registry.addViewController("/sushilite/JR1").setViewName("redirect:/sushilite/JR1/");
        registry.addViewController("/sushilite/JR1/").setViewName("forward:/sushilite/JR1/index.html");
        registry.addViewController("/sushilite/BR1").setViewName("redirect:/sushilite/BR1/");
        registry.addViewController("/sushilite/BR1/").setViewName("forward:/sushilite/BR1/index.html");
        registry.addViewController("/sushilite/BR2").setViewName("redirect:/sushilite/BR2/");
        registry.addViewController("/sushilite/BR2/").setViewName("forward:/sushilite/BR2/index.html");
        super.addViewControllers(registry);
    }
}