package eu.dnetlib.usagestats.repositories;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.dnetlib.usagestats.portal.CountryRepositories;
import eu.dnetlib.usagestats.portal.CountryUsageStats;
import eu.dnetlib.usagestats.portal.CountryUsageStatsAll;
import eu.dnetlib.usagestats.portal.MonthlyStats;
import eu.dnetlib.usagestats.portal.MonthlyUsageStats;
import eu.dnetlib.usagestats.portal.RepositoryStats;
import eu.dnetlib.usagestats.portal.TotalStats;
import eu.dnetlib.usagestats.portal.TotalStatsReposViewsDownloads;
import eu.dnetlib.usagestats.portal.UsageStats;
import eu.dnetlib.usagestats.portal.YearlyStats;
import eu.dnetlib.usagestats.sushilite.domain.ItemIdentifier;
import eu.dnetlib.usagestats.sushilite.domain.ItemPerformance;
import eu.dnetlib.usagestats.sushilite.domain.ReportItem;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.sql.DataSource;
import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UsageStatsRepository {
    private final DataSource usageStatsDB;
    private final HashOperations<String, String, String> jedis;
    private final Logger log = Logger.getLogger(this.getClass());
    @Value("${prod.statsdb}")
    private String statsDB;
    @Value("${prod.usagestatsImpalaDB}")
    private String usagestatsImpalaDB;

    public UsageStatsRepository(DataSource usageStatsDB, RedisTemplate<String, String> redisTemplate) {
        this.usageStatsDB = usageStatsDB;
        this.jedis = redisTemplate.opsForHash();
    }

    private static String MD5(String string) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(string.getBytes());
        byte[] byteData = md.digest();
        StringBuilder sb = new StringBuilder();
        byte[] var4 = byteData;
        int var5 = byteData.length;

        for(int var6 = 0; var6 < var5; ++var6) {
            byte aByteData = var4[var6];
            sb.append(Integer.toString((aByteData & 255) + 256, 16).substring(1));
        }

        return sb.toString();
    }

    private static String toJson(Object o) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(o);
    }

    private static UsageStats fromJson(String string) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return (UsageStats)objectMapper.readValue(string, UsageStats.class);
    }

    public List<MonthlyUsageStats> executeMontlyUsageStats(String query) {
        List<MonthlyUsageStats> montlhyList = new ArrayList();
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            connection = this.usageStatsDB.getConnection();
            this.log.info(connection.toString());
            st = connection.prepareStatement(query);
            this.log.info(st.toString());
            rs = st.executeQuery();

            while(rs.next()) {
                MonthlyUsageStats monthlyUsageStats = new MonthlyUsageStats();
                monthlyUsageStats.addDate(rs.getString(1));
                monthlyUsageStats.addDownloads(rs.getString(2));
                monthlyUsageStats.addViews(rs.getString(3));
                montlhyList.add(monthlyUsageStats);
            }
        } catch (Exception var13) {
            System.out.println(var13);
        }

        try {
            this.jedis.put("test", "result", toJson(montlhyList));
            this.jedis.put("test", "persistent", "false");
            this.jedis.put("test", "fetchMode", "3");
        } catch (Exception var11) {
            System.out.println(var11);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }

        return montlhyList;
    }

    public TotalStatsReposViewsDownloads executeTotalStatsReposViewsDownloads(String query) {
        TotalStatsReposViewsDownloads totalStatsReposViewsDownlads = new TotalStatsReposViewsDownloads();
        String total_repos = " ";
        String views = " ";
        String downloads = " ";
        String redis_key = "";
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            connection = this.usageStatsDB.getConnection();
            this.log.info(connection.toString());
            st = connection.prepareStatement(query);
            this.log.info(st.toString());
            rs = st.executeQuery();
            redis_key = MD5(st.toString());

            while(rs.next()) {
                totalStatsReposViewsDownlads.addRepositories(rs.getString(1));
                totalStatsReposViewsDownlads.addViews(rs.getString(2));
                totalStatsReposViewsDownlads.addDownloads(rs.getString(3));
            }
        } catch (Exception var17) {
            System.out.println(var17);
        }

        try {
            this.jedis.put(redis_key, "result", toJson(totalStatsReposViewsDownlads));
            this.jedis.put(redis_key, "persistent", "false");
            this.jedis.put(redis_key, "fetchMode", "3");
        } catch (Exception var15) {
            System.out.println(var15);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }

        return totalStatsReposViewsDownlads;
    }

    public CountryUsageStatsAll executeCountryUsageStats(String query) {
        CountryUsageStatsAll countryListAll = new CountryUsageStatsAll();
        List<CountryUsageStats> countryList = new ArrayList();
        String date = " ";
        String total_repos = " ";
        String views = " ";
        String downloads = " ";
        String redis_key = "redis_key";
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        int total_views = 0;
        int total_downloads = 0;

        try {
            connection = this.usageStatsDB.getConnection();
            this.log.info(connection.toString());
            st = connection.prepareStatement(query);
            this.log.info(st.toString());
            rs = st.executeQuery();
            redis_key = MD5(st.toString());

            while(rs.next()) {
                CountryUsageStats countryUsageStats = new CountryUsageStats();
                countryUsageStats.addCountry(rs.getString(1));
                countryUsageStats.addTotalRepos(rs.getString(2));
                countryUsageStats.addViews(rs.getString(3));
                countryUsageStats.addDownloads(rs.getString(4));
                total_views += Integer.parseInt(rs.getString(3));
                total_downloads += Integer.parseInt(rs.getString(4));
                countryList.add(countryUsageStats);
            }

            countryListAll.addViewsAll(Integer.toString(total_views));
            countryListAll.addDownloadsAll(Integer.toString(total_downloads));
            countryListAll.addCountryUsageStats(countryList);
        } catch (Exception var21) {
            this.log.info(var21);
            System.out.println(var21);
        }

        try {
            this.jedis.put(redis_key, "result", toJson(countryListAll));
            this.jedis.put(redis_key, "persistent", "false");
            this.jedis.put(redis_key, "fetchMode", "3");
        } catch (Exception var19) {
            System.out.println(var19);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }

        return countryListAll;
    }

    public CountryUsageStats executeCountryUsageStats(String query, String country) {
        CountryUsageStats countryUsageStats = new CountryUsageStats();
        String total_repos = " ";
        String views = " ";
        String downloads = " ";
        String redis_key = "";
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        int total_views = 0;
        boolean var12 = false;

        try {
            connection = this.usageStatsDB.getConnection();
            st = connection.prepareStatement(query);
            redis_key = MD5(st.toString());
            this.log.info(st.toString());
            rs = st.executeQuery();

            while(rs.next()) {
                countryUsageStats.addCountry(country);
                countryUsageStats.addTotalRepos(rs.getString(1));
                countryUsageStats.addViews(rs.getString(2));
                countryUsageStats.addDownloads(rs.getString(3));
            }
        } catch (Exception var20) {
            this.log.info(var20);
            System.out.println(var20);
        }

        try {
            this.jedis.put(redis_key, "result", toJson(countryUsageStats));
            this.jedis.put(redis_key, "persistent", "false");
            this.jedis.put(redis_key, "fetchMode", "3");
        } catch (Exception var18) {
            System.out.println(var18);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }

        return countryUsageStats;
    }

    public List<CountryRepositories> executeCountryRepositories(String query) {
        List<CountryRepositories> countryReposList = new ArrayList();
        String country = " ";
        String repository = " ";
        String redis_key = "";
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            connection = this.usageStatsDB.getConnection();
            this.log.info(connection.toString());
            st = connection.prepareStatement(query);
            this.log.info(st.toString());
            rs = st.executeQuery();
            redis_key = MD5(st.toString());

            while(rs.next()) {
                CountryRepositories countryRepository = new CountryRepositories();
                countryRepository.addCountry(rs.getString(1));
                countryRepository.addRepository(rs.getString(2));
                countryReposList.add(countryRepository);
            }
        } catch (Exception var16) {
            System.out.println(var16);
        }

        try {
            this.jedis.put(redis_key, "result", toJson(countryReposList));
            this.jedis.put(redis_key, "persistent", "false");
            this.jedis.put(redis_key, "fetchMode", "3");
        } catch (Exception var14) {
            System.out.println(var14);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }

        return countryReposList;
    }

    public List<MonthlyUsageStats> executeMontlyUsageStatsForRepo(String query, String datasourceId) {
        List<MonthlyUsageStats> montlhyList = new ArrayList();
        String redis_key = "";
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            connection = this.usageStatsDB.getConnection();
            st = connection.prepareStatement(query);
            redis_key = MD5(st.toString());
            st.setString(1, datasourceId);
            this.log.info(connection.toString());
            rs = st.executeQuery();

            while(rs.next()) {
                MonthlyUsageStats monthlyUsageStats = new MonthlyUsageStats();
                monthlyUsageStats.addDate(rs.getString(1));
                monthlyUsageStats.addDownloads(rs.getString(2));
                monthlyUsageStats.addViews(rs.getString(3));
                montlhyList.add(monthlyUsageStats);
            }
        } catch (Exception var15) {
            System.out.println(var15);
        }

        try {
            this.jedis.put(redis_key, "result", toJson(montlhyList));
            this.jedis.put(redis_key, "persistent", "false");
            this.jedis.put(redis_key, "fetchMode", "3");
        } catch (Exception var13) {
            System.out.println(var13);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }

        return montlhyList;
    }

    public UsageStats executeUsageStats(String query, List<String> values, String type) {
        UsageStats usageStats = new UsageStats();
        int total_views = 0;
        int total_downloads = 0;
        int page_views = 0;
        int openaire_downloads = 0;
        int openaire_views = 0;
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        UsageStats var16;
        try {
            connection = this.usageStatsDB.getConnection();
            st = connection.prepareStatement(query);
            int i = 1;

            String redis_result;
            for(Iterator var14 = values.iterator(); var14.hasNext(); ++i) {
                redis_result = (String)var14.next();
                st.setString(i, redis_result);
            }

            String redis_key = MD5(st.toString());
            redis_result = (String)this.jedis.get(redis_key, "result");
            if (redis_result == null) {
                rs = st.executeQuery();
                if (type.equals("result")) {
                    while(true) {
                        while(rs.next()) {
                            if (rs.getString(1).equals("views") && rs.getString(4) != null && !rs.getString(4).equals("") && !rs.getString(4).equals("null")) {
                                usageStats.addViews(new RepositoryStats(rs.getString(3), rs.getString(2), rs.getString(4), rs.getString(5)));
                                total_views += Integer.parseInt(rs.getString(4));
                                openaire_views += Integer.parseInt(rs.getString(5));
                            } else if (rs.getString(1).equals("downloads") && rs.getString(4) != null && !rs.getString(4).equals("") && !rs.getString(4).equals("null")) {
                                usageStats.addDownloads(new RepositoryStats(rs.getString(3), rs.getString(2), rs.getString(4), "0"));
                                total_downloads += Integer.parseInt(rs.getString(4));
                                openaire_downloads += Integer.parseInt(rs.getString(5));
                            } else if (rs.getString(1).equals("pageviews") && rs.getString(4) != null && !rs.getString(4).equals("") && !rs.getString(4).equals("null")) {
                                page_views = Integer.parseInt(rs.getString(4));
                            }
                        }

                        usageStats.setTotal_views(Integer.toString(total_views));
                        usageStats.setTotal_downloads(Integer.toString(total_downloads));
                        usageStats.setPageViews(Integer.toString(page_views));
                        usageStats.setTotal_openaire_views(Integer.toString(openaire_views));
                        usageStats.setTotal_openaire_downloads(Integer.toString(openaire_downloads));
                        break;
                    }
                } else if (type.equals("project") || type.equals("datasource")) {
                    while(true) {
                        while(rs.next()) {
                            if (rs.getString(1).equals("views") && rs.getString(2) != null && !rs.getString(2).equals("") && !rs.getString(2).equals("null")) {
                                total_views += Integer.parseInt(rs.getString(2));
                                openaire_views += Integer.parseInt(rs.getString(3));
                            } else if (rs.getString(1).equals("downloads") && rs.getString(2) != null && !rs.getString(2).equals("") && !rs.getString(2).equals("null")) {
                                total_downloads += Integer.parseInt(rs.getString(2));
                                openaire_downloads += Integer.parseInt(rs.getString(3));
                            } else if (rs.getString(1).equals("pageviews") && rs.getString(2) != null && !rs.getString(2).equals("") && !rs.getString(2).equals("null")) {
                                page_views = Integer.parseInt(rs.getString(2));
                            }
                        }

                        usageStats.setTotal_views(Integer.toString(total_views));
                        usageStats.setTotal_downloads(Integer.toString(total_downloads));
                        usageStats.setPageViews(Integer.toString(page_views));
                        usageStats.setTotal_openaire_views(Integer.toString(openaire_views));
                        usageStats.setTotal_openaire_downloads(Integer.toString(openaire_downloads));
                        break;
                    }
                }

                this.jedis.put(redis_key, "persistent", "false");
                this.jedis.put(redis_key, "query", st.toString());
                this.jedis.put(redis_key, "result", toJson(usageStats));
                this.jedis.put(redis_key, "fetchMode", "3");
                return usageStats;
            }

            var16 = fromJson(redis_result);
        } catch (Exception var20) {
            this.log.error("Cannot execute query2 : ", var20);
            return usageStats;
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }

        return var16;
    }

    public TotalStats executeTotalStats() {
        TotalStats totalStats = null;

        try {
            String redis_result = (String)this.jedis.get("total_stats", "result");
            if (redis_result == null) {
                return this.updateTotalStats();
            }

            totalStats = fromJsonTotalStats(redis_result);
        } catch (Exception var3) {
            this.log.error("Cannot execute totalStats : ", var3);
        }

        return totalStats;
    }

    public TotalStats updateTotalStats() {
        TotalStats totalStats = new TotalStats();
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        HashMap monthlyStatsMap = new HashMap();

        try {
            connection = this.usageStatsDB.getConnection();
            st = connection.prepareStatement("SELECT ndv(distinct repository_id) AS repository, ndv(distinct result_id) AS items, sum(downloads) AS downloads, sum(views) AS views FROM " + this.usagestatsImpalaDB + ".usage_stats;");
            rs = st.executeQuery();
            rs.next();
            totalStats.setRepositories(rs.getInt(1));
            totalStats.setItems(rs.getInt(2));
            totalStats.setDownloads(rs.getInt(3));
            totalStats.setViews(rs.getInt(4));
            rs.close();
            st.close();
            st = connection.prepareStatement("SELECT `date`, ndv(distinct repository_id) AS repository, ndv(distinct result_id) AS items, sum(downloads) AS downloads, sum(views) AS views FROM " + this.usagestatsImpalaDB + ".usage_stats GROUP BY `date` ORDER BY `date`;");
            rs = st.executeQuery();

            while(rs.next()) {
                int year = Integer.parseInt(rs.getString(1).substring(0, 4));
                int month = Integer.parseInt(rs.getString(1).substring(5));
                MonthlyStats monthlyStats = new MonthlyStats();
                monthlyStats.setMonth(month);
                monthlyStats.setRepositories(rs.getInt(2));
                monthlyStats.setItems(rs.getInt(3));
                monthlyStats.setDownloads(rs.getInt(4));
                monthlyStats.setViews(rs.getInt(5));
                if (monthlyStatsMap.get(year) != null) {
                    ((List)monthlyStatsMap.get(year)).add(monthlyStats);
                } else {
                    List<MonthlyStats> newList = new ArrayList();
                    newList.add(monthlyStats);
                    monthlyStatsMap.put(year, newList);
                }
            }

            rs.close();
            st.close();
            st = connection.prepareStatement("SELECT SUBSTR(`date`,1,4) AS year, ndv(DISTINCT repository_id) AS repository, \nndv(DISTINCT result_id) AS items, SUM(downloads) AS downloads, SUM(views) AS views \nFROM " + this.usagestatsImpalaDB + ".usage_stats GROUP BY year ORDER BY year;");
            rs = st.executeQuery();
            ArrayList yearlyStatsList = new ArrayList();

            while(rs.next()) {
                YearlyStats yearlyStats = new YearlyStats();
                yearlyStats.setYear(rs.getInt(1));
                yearlyStats.setRepositories(rs.getInt(2));
                yearlyStats.setItems(rs.getInt(3));
                yearlyStats.setDownloads(rs.getInt(4));
                yearlyStats.setViews(rs.getInt(5));
                yearlyStats.setMonthlyStats((List)monthlyStatsMap.get(rs.getInt(1)));
                yearlyStatsList.add(yearlyStats);
            }

            totalStats.setYearlyStats(yearlyStatsList);
//            this.jedis.put("total_stats", "result", toJson(totalStats));
//            this.jedis.put("total_stats", "persistent", "false");
        } catch (Exception var13) {
            this.log.error("Cannot execute totalStats : ", var13);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }

        return totalStats;
    }

    private static TotalStats fromJsonTotalStats(String string) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return (TotalStats)objectMapper.readValue(string, TotalStats.class);
    }

    public String executeRepoId(String repositoryIdentifier, String report) {
        PreparedStatement st = null;
        Connection connection = null;
        ResultSet rs = null;
        this.log.info("database " + this.statsDB);

        try {
            connection = this.usageStatsDB.getConnection();
            String[] split = repositoryIdentifier.split(":");
            String openaire_id = "-1";
            String var8 = split[0].toLowerCase();
            byte var9 = -1;
            switch(var8.hashCode()) {
            case -504258139:
                if (var8.equals("openaire")) {
                    var9 = 0;
                }
                break;
            case -504163514:
                if (var8.equals("opendoar")) {
                    var9 = 1;
                }
                break;
            case 3242245:
                if (var8.equals("issn")) {
                    var9 = 2;
                }
            }

            String var10;
            switch(var9) {
            case 0:
                if (!report.equals("jr1")) {
                    st = connection.prepareStatement("select id from " + this.statsDB + ".datasource where id=?");
                    st.setString(1, repositoryIdentifier.replaceFirst(split[0] + ":", ""));
                } else {
                    st = connection.prepareStatement("select id from " + this.statsDB + ".datasource where id=? AND (type='Journal' OR type='Journal Aggregator/Publisher')");
                    st.setString(1, repositoryIdentifier.replaceFirst(split[0] + ":", ""));
                }

                for(rs = st.executeQuery(); rs.next(); openaire_id = rs.getString(1)) {
                }

                var10 = openaire_id;
                return var10;
            case 1:
                if (!report.equals("jr1")) {
                    st = connection.prepareStatement("select id from " + this.statsDB + ".datasource_oids where oid=?");
                    st.setString(1, "opendoar____::" + repositoryIdentifier.replaceFirst(split[0] + ":", ""));
                } else {
                    st = connection.prepareStatement("select distinct d.id from " + this.statsDB + ".datasource d, " + this.statsDB + ".datasource_oids di where di.oid=? and d.id=di.id and (type='Journal' OR type='Journal Aggregator/Publisher')");
                    st.setString(1, "opendoar____::" + repositoryIdentifier.replaceFirst(split[0] + ":", ""));
                }

                for(rs = st.executeQuery(); rs.next(); openaire_id = rs.getString(1)) {
                }

                var10 = openaire_id;
                return var10;
            case 2:
                st = connection.prepareStatement("select distinct d.id from " + this.statsDB + ".datasource d, " + this.statsDB + ".datasource_oids di, " + this.statsDB + ".datasource_results dr where d.id=dr.id and di.oid like ? and d.id=di.id and (type='Journal' OR type='Journal Aggregator/Publisher')");
                st.setString(1, "%" + repositoryIdentifier.replaceFirst(split[0] + ":", "") + "%");

                for(rs = st.executeQuery(); rs.next(); openaire_id = rs.getString(1)) {
                }

                var10 = openaire_id;
                return var10;
            default:
                var10 = "-1";
                return var10;
            }
        } catch (Exception var14) {
            this.log.error("Repository id failed: ", var14);
            return "-1";
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }
    }

    public void executeItem(List<ReportItem> reportItems, String itemIdentifier, String repositoryIdentifier, String itemDataType, Date beginDate, Date endDate, String granularity) {
        String[] split = itemIdentifier.split(":");
        String var9 = split[0].toLowerCase();
        byte var10 = -1;
        switch(var9.hashCode()) {
        case -504258139:
            if (var9.equals("openaire")) {
                var10 = 2;
            }
            break;
        case 99646:
            if (var9.equals("doi")) {
                var10 = 1;
            }
            break;
        case 110026:
            if (var9.equals("oid")) {
                var10 = 0;
            }
        }

        switch(var10) {
        case 0:
            this.executeOid(reportItems, itemIdentifier.replaceFirst(split[0] + ":", ""), repositoryIdentifier, itemDataType, beginDate, endDate, granularity);
            break;
        case 1:
            this.executeDoi(reportItems, itemIdentifier.replaceFirst(split[0] + ":", ""), repositoryIdentifier, itemDataType, beginDate, endDate, granularity);
            break;
        case 2:
            this.executeOpenaire(reportItems, itemIdentifier.replaceFirst(split[0] + ":", ""), repositoryIdentifier, itemDataType, beginDate, endDate, granularity);
        }

    }

    private void executeOid(List<ReportItem> reportItems, String oid, String repositoryIdentifier, String itemDataType, Date beginDate, Date endDate, String granularity) {
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            connection = this.usageStatsDB.getConnection();
            st = connection.prepareStatement("SELECT DISTINCT roid.id FROM " + this.statsDB + ".result_oids roid, " + this.usagestatsImpalaDB + ".usage_stats us WHERE us.result_id=roid.id AND roid.oid=?");
            st.setString(1, oid);
            rs = st.executeQuery();

            while(rs.next()) {
                this.executeOpenaire(reportItems, rs.getString(1), repositoryIdentifier, itemDataType, beginDate, endDate, granularity);
            }

            connection.close();
        } catch (Exception var15) {
            this.log.error("Oid to OpenAIRE id failed: ", var15);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }

    }

    private void executeDoi(List<ReportItem> reportItems, String doi, String repositoryIdentifier, String itemDataType, Date beginDate, Date endDate, String granularity) {
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            connection = this.usageStatsDB.getConnection();
            //st = connection.prepareStatement("SELECT DISTINCT poid.id FROM " + this.statsDB + ".result_pids poid, " + this.usageStatsDB + ".usage_stats us WHERE us.result_id=poid.id AND poid.type='Digital Object Identifier' AND poid.pid=?");
            st = connection.prepareStatement("SELECT DISTINCT poid.id FROM " + this.statsDB + ".result_pids poid WHERE poid.type='Digital Object Identifier' AND poid.pid=?");
            st.setString(1, doi);
            rs = st.executeQuery();

            while(rs.next()) {
                this.executeOpenaire(reportItems, rs.getString(1), repositoryIdentifier, itemDataType, beginDate, endDate, granularity);
            }
        } catch (Exception var15) {
            this.log.error("Doi to OpenAIRE id failed: ", var15);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }

    }

    private void executeOpenaire(List<ReportItem> reportItems, String openaire, String repositoryIdentifier, String itemDataType, Date beginDate, Date endDate, String granularity) {
        SimpleDateFormat report_dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat postgresFormat = new SimpleDateFormat("yyyy/MM");
        String beginDateStr = postgresFormat.format(beginDate);
        String endDateStr = postgresFormat.format(endDate);
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            connection = this.usageStatsDB.getConnection();
            if (repositoryIdentifier.equals("")) {
                if (itemDataType.equals("")) {
                    st = connection.prepareStatement("SELECT distinct res.repository_id, r.title, r.publisher, r.source, rc.type, pids.pid, d.name, res.`date`, oids.oid, res.downloads, res.views FROM (SELECT us.repository_id, us.result_id, us.`date`, us.downloads, us.views FROM " + this.usagestatsImpalaDB + ".usage_stats us WHERE us.`date`>=? AND us.`date`<=? AND us.result_id=?) AS res JOIN " + this.statsDB + ".result r ON res.result_id=r.id JOIN " + this.statsDB + ".datasource d ON d.id=res.repository_id JOIN (select id, group_concat(type,',') as type FROM " + this.statsDB + ".result_classifications where id=? GROUP by id) rc ON rc.id=r.id LEFT JOIN (SELECT pids.id, group_concat(pids.pid, '#!#') AS pid FROM " + this.statsDB + ".result_pids pids WHERE pids.id=? AND type='Digital Object Identifier' GROUP BY pids.id) AS pids ON pids.id=r.id LEFT JOIN (SELECT id, group_concat(oids.oid, '#!#') AS oid FROM " + this.statsDB + ".result_oids oids WHERE oids.id=? GROUP BY oids.id) AS oids ON oids.id=r.id ORDER BY res.repository_id, res.`date`;");
                    st.setString(1, beginDateStr);
                    st.setString(2, endDateStr);
                    st.setString(3, openaire);
                    st.setString(4, openaire);
                    st.setString(5, openaire);
                    st.setString(6, openaire);
                } else {
                    st = connection.prepareStatement("SELECT distinct res.repository_id, r.title, r.publisher, r.source, rc.type, pids.pid, d.name, res.`date`, oids.oid, res.downloads, res.views FROM (SELECT us.repository_id, us.result_id, us.`date`, us.downloads, us.views FROM " + this.usagestatsImpalaDB + ".usage_stats us WHERE us.`date`>=? AND us.`date`<=? AND us.result_id=?) AS res JOIN " + this.statsDB + ".result r ON res.result_id=r.id JOIN " + this.statsDB + ".datasource d ON d.id=res.repository_id JOIN " + this.statsDB + ".result_classifications rc ON rc.id=r.id AND rc.type=? LEFT JOIN (SELECT pids.id, group_concat(pids.pid, '#!#') AS pid FROM " + this.statsDB + ".result_pids pids WHERE pids.id=? AND type='Digital Object Identifier' GROUP BY pids.id) AS pids ON pids.id=r.id LEFT JOIN (SELECT oids.id, group_concat(oids.oid, '#!#') AS oid FROM " + this.statsDB + ".result_oids oids WHERE oids.id=? GROUP BY oids.id) AS oids ON oids.id=r.id ORDER BY res.repository_id, res.`date`;");
                    st.setString(1, beginDateStr);
                    st.setString(2, endDateStr);
                    st.setString(3, openaire);
                    st.setString(4, itemDataType);
                    st.setString(5, openaire);
                    st.setString(6, openaire);
                }
            } else if (itemDataType.equals("")) {
                st = connection.prepareStatement("SELECT distinct res.repository_id, r.title, r.publisher, r.source, rc.type, pids.pid, d.name, res.`date`, oids.oid, res.downloads, res.views FROM (SELECT us.repository_id, us.result_id, us.`date`, us.downloads, us.views FROM " + this.usagestatsImpalaDB + ".usage_stats us WHERE us.`date`>=? AND us.`date`<=? AND us.result_id=? AND us.repository_id=?) AS res JOIN " + this.statsDB + ".result r ON res.result_id=r.id JOIN " + this.statsDB + ".datasource d ON d.id=res.repository_id JOIN (select id, group_concat(type,',') as type from " + this.statsDB + ".result_classifications where id=? group by id) rc ON rc.id=r.id LEFT JOIN (SELECT pids.id, group_concat(pids.pid, '#!#') AS pid FROM " + this.statsDB + ".result_pids pids WHERE pids.id=? AND type='Digital Object Identifier' GROUP BY pids.id) AS pids ON pids.id=r.id LEFT JOIN (SELECT oids.id, group_concat(oids.oid, '#!#') AS oid FROM " + this.statsDB + ".result_oids oids WHERE oids.id=? GROUP BY oids.id) AS oids ON oids.id=r.id ORDER BY res.repository_id, res.`date`;");
                st.setString(1, beginDateStr);
                st.setString(2, endDateStr);
                st.setString(3, openaire);
                st.setString(4, repositoryIdentifier);
                st.setString(5, openaire);
                st.setString(6, openaire);
                st.setString(7, openaire);
            } else {
                st = connection.prepareStatement("SELECT distinct res.repository_id, r.title, r.publisher, r.source, rc.type, pids.pid, d.name, res.`date`, oids.oid, res.downloads,res.views FROM (SELECT us.repository_id, us.result_id, us.`date`, us.downloads, us.views FROM " + this.usagestatsImpalaDB + ".usage_stats us WHERE us.`date`>=? AND us.`date`<=? AND us.result_id=? AND us.repository_id=?) AS res JOIN " + this.statsDB + ".result r ON res.result_id=r.id JOIN " + this.statsDB + ".datasource d ON d.id=res.repository_id JOIN " + this.statsDB + ".result_classifications rc ON rc.id=r.id AND rc.type=? LEFT JOIN (SELECT pids.id, group_concat(pids.pid, '#!#') AS pid FROM " + this.statsDB + ".result_pids pids WHERE pids.id=? AND type='Digital Object Identifier' GROUP BY pids.id) AS pids ON pids.id=r.id LEFT JOIN (SELECT oids.id, group_concat(oids.oid, '#!#') AS oid FROM " + this.statsDB + ".result_oids oids WHERE oids.id=? GROUP BY oids.id) AS oids ON oids.id=r.id ORDER BY res.repository_id, res.`date`;");
                st.setString(1, beginDateStr);
                st.setString(2, endDateStr);
                st.setString(3, openaire);
                st.setString(4, repositoryIdentifier);
                st.setString(5, itemDataType);
                st.setString(6, openaire);
                st.setString(7, openaire);
            }

            rs = st.executeQuery();
            String repository = "";
            String lastDate = "";
            ReportItem reportItem = null;
            int ft_total = 0;
            int abstr = 0;
            if (!granularity.equalsIgnoreCase("totals")) {
                if (granularity.equalsIgnoreCase("monthly")) {
                    Calendar endCal = Calendar.getInstance();
                    endCal.setTime(postgresFormat.parse(endDateStr));
                    endCal.add(2, 1);

                    Date endDateForZeros;
                    Calendar endC;
                    for(endDateForZeros = endCal.getTime(); rs.next(); lastDate = postgresFormat.format(endC.getTime())) {
                        if (!rs.getString(1).equals(repository)) {
                            if (reportItem != null) {
                                this.fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
                                reportItems.add(reportItem);
                            }

                            repository = rs.getString(1);
                            lastDate = beginDateStr;
                            reportItem = new ReportItem(rs.getString(3), rs.getString(7), rs.getString(5), rs.getString(2));
                            reportItem.addIdentifier(new ItemIdentifier("OpenAIRE", openaire));
                            reportItem.addIdentifier(new ItemIdentifier("URLs", rs.getString(4)));
                            if (rs.getString(9) != null && !rs.getString(9).equals("")) {
                                if (rs.getString(9).contains("#!#")) {
                                    String allOAIs = rs.getString(9);
                                    String[] oaiArray = allOAIs.split("#!#");

                                    for(int i = 0; i < oaiArray.length; ++i) {
                                        reportItem.addIdentifier(new ItemIdentifier("OAI", oaiArray[i]));
                                    }
                                } else {
                                    reportItem.addIdentifier(new ItemIdentifier("OAI", rs.getString(9)));
                                }
                            }

                            if (rs.getString(6) != null && !rs.getString(6).equals("")) {
                                if (rs.getString(6).contains("#!#")) {
                                    reportItem.addIdentifier(new ItemIdentifier("DOI", rs.getString(6).substring(0, rs.getString(6).indexOf("#!#"))));
                                } else {
                                    reportItem.addIdentifier(new ItemIdentifier("DOI", rs.getString(6)));
                                }
                            }
                        }

                        this.fillWithZeros(postgresFormat.parse(lastDate), postgresFormat.parse(rs.getString(8)), reportItem);
                        endC = Calendar.getInstance();
                        endC.setTime(postgresFormat.parse(rs.getString(8)));
                        endC.set(5, endC.getActualMaximum(5));
                        if (reportItem != null) {
                            reportItem.addPerformance(new ItemPerformance(report_dateFormat.format(postgresFormat.parse(rs.getString(8))), report_dateFormat.format(endC.getTime()), rs.getString(10), rs.getString(11)));
                        }

                        endC.setTime(postgresFormat.parse(rs.getString(8)));
                        endC.add(2, 1);
                    }

                    if (reportItem != null) {
                        this.fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
                        reportItems.add(reportItem);
                    }
                }
            } else {
                while(rs.next()) {
                    if (!rs.getString(1).equals(repository)) {
                        if (reportItem != null) {
                            reportItem.addPerformance(new ItemPerformance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate), Integer.toString(ft_total), Integer.toString(abstr)));
                            reportItems.add(reportItem);
                        }

                        repository = rs.getString(1);
                        reportItem = new ReportItem(rs.getString(3), rs.getString(7), rs.getString(5), rs.getString(2));
                        reportItem.addIdentifier(new ItemIdentifier("OpenAIRE", openaire));
                        reportItem.addIdentifier(new ItemIdentifier("URLs", rs.getString(4)));
                        if (rs.getString(9) != null && !rs.getString(9).equals("")) {
                            if (rs.getString(9).contains("#!#")) {
                                reportItem.addIdentifier(new ItemIdentifier("OAI", rs.getString(9).substring(0, rs.getString(9).indexOf("#!#"))));
                            } else {
                                reportItem.addIdentifier(new ItemIdentifier("OAI", rs.getString(9)));
                            }
                        }

                        if (rs.getString(6) != null && !rs.getString(6).equals("")) {
                            if (rs.getString(6).contains("#!#")) {
                                reportItem.addIdentifier(new ItemIdentifier("DOI", rs.getString(6).substring(0, rs.getString(6).indexOf("#!#"))));
                            } else {
                                reportItem.addIdentifier(new ItemIdentifier("DOI", rs.getString(6)));
                            }
                        }

                        ft_total = 0;
                        abstr = 0;
                    }

                    ft_total += rs.getInt(10);
                    abstr += rs.getInt(11);
                }

                if (reportItem != null) {
                    reportItem.addPerformance(new ItemPerformance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate), Integer.toString(ft_total), Integer.toString(abstr)));
                    reportItems.add(reportItem);
                }
            }
        } catch (Exception var28) {
            this.log.error("Single Item Report failed: ", var28);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }

    }

    public void executeRepo(List<ReportItem> reportItems, String repositoryIdentifier, String itemDataType, Date beginDate, Date endDate, String granularity) {
        SimpleDateFormat report_dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat postgresFormat = new SimpleDateFormat("yyyy/MM");
        String beginDateStr = postgresFormat.format(beginDate);
        String endDateStr = postgresFormat.format(endDate);
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            connection = this.usageStatsDB.getConnection();
            if (repositoryIdentifier.equals("")) {
                if (itemDataType.equals("")) {
                    st = connection.prepareStatement("SELECT d.id, d.name, d.websiteurl, dois.oid, res.`date`, res.downloads, res.views FROM (SELECT us.source, us.repository_id, us.`date`, sum(us.downloads) AS downloads, sum(us.views) AS views FROM " + this.usagestatsImpalaDB + ".usage_stats us WHERE us.`date`>=? AND us.`date`<=? GROUP BY us.source, us.repository_id, us.`date`) AS res JOIN " + this.statsDB + ".datasource d ON d.id=res.repository_id JOIN " + this.statsDB + ".datasource_oids dois ON d.id=dois.id WHERE dois.oid LIKE 'opendoar%' ORDER BY d.id, d.name, res.`date` ASC;");
                    st.setString(1, beginDateStr);
                    st.setString(2, endDateStr);
                } else {
                    st = connection.prepareStatement("SELECT d.id, d.name, d.websiteurl, dois.oid, res.`date`, res.downloads, res.views FROM (SELECT us.source, us.repository_id, us.`date`, sum(us.downloads) AS downloads, sum(us.views) AS views FROM " + this.usagestatsImpalaDB + ".usage_stats us, " + this.statsDB + ".result_classifications rc WHERE rc.id=us.result_id AND us.`date`>=? AND us.`date`<=? AND rc.type=? GROUP BY us.source, us.repository_id, us.`date`) AS res JOIN " + this.statsDB + ".datasource d ON d.id=res.repository_id JOIN " + this.statsDB + ".datasource_oids dois ON d.id=dois.id WHERE dois.oid LIKE 'opendoar%' ORDER BY d.id, d.name, res.`date` ASC;");
                    st.setString(1, beginDateStr);
                    st.setString(2, endDateStr);
                    st.setString(3, itemDataType);
                }
            } else if (itemDataType.equals("")) {
                st = connection.prepareStatement("SELECT d.id, d.name, d.websiteurl, dois.oid, res.`date`, res.downloads, res.views FROM (SELECT us.source, us.repository_id, us.`date`, sum(us.downloads) AS downloads, sum(us.views) AS views FROM " + this.usagestatsImpalaDB + ".usage_stats us WHERE us.`date`>=? AND us.`date`<=? AND us.repository_id=? GROUP BY us.source, us.repository_id, us.`date`) AS res JOIN " + this.statsDB + ".datasource d ON d.id=res.repository_id JOIN " + this.statsDB + ".datasource_oids dois ON d.id=dois.id WHERE dois.oid LIKE 'opendoar%' ORDER BY d.id, d.name, res.`date` ASC;");
                st.setString(1, beginDateStr);
                st.setString(2, endDateStr);
                st.setString(3, repositoryIdentifier);
            } else {
                st = connection.prepareStatement("SELECT d.id, d.name, d.websiteurl, dois.oid, res.`date`, res.downloads, res.views FROM (SELECT us.source, us.repository_id, us.`date`, sum(us.downloads) AS downloads, sum(us.views) AS views FROM " + this.usagestatsImpalaDB + ".usage_stats us, " + this.statsDB + ".result_classifications rc WHERE rc.id=us.result_id AND us.`date`>=? AND us.`date`<=? AND rc.type=? AND us.repository_id=? GROUP BY us.source, us.repository_id, us.`date`) AS res JOIN " + this.statsDB + ".datasource d ON d.id=res.repository_id JOIN " + this.statsDB + ".datasource_oids dois ON d.id=dois.id WHERE dois.oid LIKE 'opendoar%' ORDER BY d.id, d.name, res.`date` ASC;");
                st.setString(1, beginDateStr);
                st.setString(2, endDateStr);
                st.setString(3, itemDataType);
                st.setString(4, repositoryIdentifier);
            }

            rs = st.executeQuery();
            String repository = "";
            String lastDate = "";
            ReportItem reportItem = null;
            int ft_total = 0;
            int abstr = 0;
            if (granularity.equalsIgnoreCase("totals")) {
                while(rs.next()) {
                    if (!rs.getString(1).equals(repository)) {
                        if (reportItem != null) {
                            reportItem.addPerformance(new ItemPerformance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate), Integer.toString(ft_total), Integer.toString(abstr)));
                            reportItems.add(reportItem);
                        }

                        repository = rs.getString(1);
                        reportItem = new ReportItem((String)null, rs.getString(2), "Platform", (String)null);
                        reportItem.addIdentifier(new ItemIdentifier("OpenAIRE", rs.getString(1)));
                        reportItem.addIdentifier(new ItemIdentifier("OpenDOAR", rs.getString(4).substring(rs.getString(4).lastIndexOf(":") + 1)));
                        reportItem.addIdentifier(new ItemIdentifier("URL", rs.getString(3)));
                        ft_total = 0;
                        abstr = 0;
                    }

                    ft_total += rs.getInt(6);
                    abstr += rs.getInt(7);
                }

                if (reportItem != null) {
                    reportItem.addPerformance(new ItemPerformance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate), Integer.toString(ft_total), Integer.toString(abstr)));
                    reportItems.add(reportItem);
                }
            } else if (granularity.equalsIgnoreCase("monthly")) {
                Calendar endCal = Calendar.getInstance();
                endCal.setTime(postgresFormat.parse(endDateStr));
                endCal.add(2, 1);

                Date endDateForZeros;
                Calendar endC;
                for(endDateForZeros = endCal.getTime(); rs.next(); lastDate = postgresFormat.format(endC.getTime())) {
                    if (!rs.getString(1).equals(repository)) {
                        if (reportItem != null) {
                            this.fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
                            reportItems.add(reportItem);
                        }

                        repository = rs.getString(1);
                        lastDate = beginDateStr;
                        reportItem = new ReportItem((String)null, rs.getString(2), "Platform", (String)null);
                        reportItem.addIdentifier(new ItemIdentifier("OpenAIRE", rs.getString(1)));
                        reportItem.addIdentifier(new ItemIdentifier("OpenDOAR", rs.getString(4).substring(rs.getString(4).lastIndexOf(":") + 1)));
                        reportItem.addIdentifier(new ItemIdentifier("URL", rs.getString(3)));
                    }

                    this.fillWithZeros(postgresFormat.parse(lastDate), postgresFormat.parse(rs.getString(5)), reportItem);
                    endC = Calendar.getInstance();
                    endC.setTime(postgresFormat.parse(rs.getString(5)));
                    endC.set(5, endC.getActualMaximum(5));
                    if (reportItem != null) {
                        reportItem.addPerformance(new ItemPerformance(report_dateFormat.format(postgresFormat.parse(rs.getString(5))), report_dateFormat.format(endC.getTime()), rs.getString(6), rs.getString(7)));
                    }

                    endC.setTime(postgresFormat.parse(rs.getString(5)));
                    endC.add(2, 1);
                }

                if (reportItem != null) {
                    this.fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
                    reportItems.add(reportItem);
                }
            }

            rs.close();
            st.close();
            connection.close();
        } catch (Exception var25) {
            this.log.error("Repository Report failed: ", var25);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }

    }

    public void executeJournal(List<ReportItem> reportItems, String repositoryIdentifier, String itemDataType, Date beginDate, Date endDate, String granularity) {
        SimpleDateFormat report_dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat postgresFormat = new SimpleDateFormat("yyyy/MM");
        String beginDateStr = postgresFormat.format(beginDate);
        String endDateStr = postgresFormat.format(endDate);
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            connection = this.usageStatsDB.getConnection();
            if (repositoryIdentifier.equals("")) {
                if (itemDataType.equals("")) {
                    st = connection.prepareStatement("SELECT d.id, d.name, d.websiteurl, dois.oid, res.`date`, res.downloads, res.views FROM (SELECT us.source, us.repository_id, us.`date`, sum(us.downloads) as downloads, sum(us.views) as views FROM " + this.usagestatsImpalaDB + ".usage_stats us WHERE us.`date`>=? AND us.`date`<=? GROUP BY us.source, us.repository_id, us.`date`) AS res JOIN " + this.statsDB + ".datasource d ON d.id=res.repository_id JOIN " + this.statsDB + ".datasource_oids dois ON d.id=dois.id WHERE (d.type='Journal' OR d.type='Journal Aggregator/Publisher') ORDER BY d.id, d.name, res.`date` ASC;");
                    st.setString(1, beginDateStr);
                    st.setString(2, endDateStr);
                } else {
                    st = connection.prepareStatement("SELECT d.id, d.name, d.websiteurl, dois.oid, res.`date`, res.downloads, res.views FROM (SELECT us.source, us.repository_id, us.`date`, sum(us.downloads) as downloads, sum(us.views) as views FROM " + this.usagestatsImpalaDB + ".usage_stats us, " + this.statsDB + ".result_classifications rc WHERE rc.id=us.result_id AND us.`date`>=? AND us.`date`<=? AND rc.type=? GROUP BY us.source, us.repository_id, us.`date`) AS res JOIN " + this.statsDB + ".datasource d ON d.id=res.repository_id JOIN " + this.statsDB + ".datasource_oids dois ON d.id=dois.id WHERE (d.type='Journal' OR d.type='Journal Aggregator/Publisher') ORDER BY d.id, d.name, res.`date` ASC;");
                    st.setString(1, beginDateStr);
                    st.setString(2, endDateStr);
                    st.setString(3, itemDataType);
                }
            } else if (itemDataType.equals("")) {
                st = connection.prepareStatement("SELECT d.id, d.name, d.websiteurl, dois.oid, res.`date`, res.downloads, res.views FROM (SELECT us.source, us.repository_id, us.`date`, sum(us.downloads) as downloads, sum(us.views) as views FROM " + this.usagestatsImpalaDB + ".usage_stats us WHERE us.`date`>=? AND us.`date`<=? AND us.repository_id=? GROUP BY us.source, us.repository_id, us.`date`) AS res JOIN " + this.statsDB + ".datasource d ON d.id=res.repository_id JOIN " + this.statsDB + ".datasource_oids dois ON d.id=dois.id WHERE (d.type='Journal' OR d.type='Journal Aggregator/Publisher') ORDER BY d.id, d.name, res.`date` ASC;");
                st.setString(1, beginDateStr);
                st.setString(2, endDateStr);
                st.setString(3, repositoryIdentifier);
            } else {
                st = connection.prepareStatement("SELECT d.id, d.name, d.websiteurl, dois.oid, res.`date`, res.downloads, res.views FROM (SELECT us.source, us.repository_id, us.`date`, sum(us.downloads) as downloads, sum(us.views) as views FROM " + this.usagestatsImpalaDB + ".usage_stats us, " + this.statsDB + ".result_classifications rc WHERE rc.id=us.result_id AND us.`date`>=? AND us.`date`<=? AND rc.type=? AND us.repository_id=? GROUP BY us.source, us.repository_id, us.`date`) AS res JOIN " + this.statsDB + ".datasource d ON d.id=res.repository_id JOIN " + this.statsDB + ".datasource_oids dois ON d.id=dois.id WHERE (d.type='Journal' OR d.type='Journal Aggregator/Publisher') ORDER BY d.id, d.name, res.`date` ASC;");
                st.setString(1, beginDateStr);
                st.setString(2, endDateStr);
                st.setString(3, itemDataType);
                st.setString(4, repositoryIdentifier);
            }

            rs = st.executeQuery();
            String repository = "";
            String lastDate = "";
            ReportItem reportItem = null;
            int ft_total = 0;
            int abstr = 0;
            if (granularity.equalsIgnoreCase("totals")) {
                while(rs.next()) {
                    if (!rs.getString(1).equals(repository)) {
                        if (reportItem != null) {
                            reportItem.addPerformance(new ItemPerformance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate), Integer.toString(ft_total), Integer.toString(abstr)));
                            reportItems.add(reportItem);
                        }

                        repository = rs.getString(1);
                        reportItem = new ReportItem((String)null, rs.getString(2), "Platform", (String)null);
                        reportItem.addIdentifier(new ItemIdentifier("OpenAIRE", rs.getString(1)));
                        reportItem.addIdentifier(new ItemIdentifier("ISSN", rs.getString(4).substring(rs.getString(4).lastIndexOf(":") + 1)));
                        if (rs.getString(3) != null) {
                            reportItem.addIdentifier(new ItemIdentifier("URL", rs.getString(3)));
                        }

                        ft_total = 0;
                        abstr = 0;
                    }

                    ft_total += rs.getInt(6);
                    abstr += rs.getInt(7);
                }

                if (reportItem != null) {
                    reportItem.addPerformance(new ItemPerformance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate), Integer.toString(ft_total), Integer.toString(abstr)));
                    reportItems.add(reportItem);
                }
            } else if (granularity.equalsIgnoreCase("monthly")) {
                Calendar endCal = Calendar.getInstance();
                endCal.setTime(postgresFormat.parse(endDateStr));
                endCal.add(2, 1);

                Date endDateForZeros;
                Calendar endC;
                for(endDateForZeros = endCal.getTime(); rs.next(); lastDate = postgresFormat.format(endC.getTime())) {
                    if (!rs.getString(1).equals(repository)) {
                        if (reportItem != null) {
                            this.fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
                            reportItems.add(reportItem);
                        }

                        repository = rs.getString(1);
                        lastDate = beginDateStr;
                        reportItem = new ReportItem((String)null, rs.getString(2), "Platform", (String)null);
                        reportItem.addIdentifier(new ItemIdentifier("OpenAIRE", rs.getString(1)));
                        reportItem.addIdentifier(new ItemIdentifier("ISSN", rs.getString(4).substring(rs.getString(4).lastIndexOf(":") + 1)));
                        if (rs.getString(3) != null) {
                            reportItem.addIdentifier(new ItemIdentifier("URL", rs.getString(3)));
                        }
                    }

                    this.fillWithZeros(postgresFormat.parse(lastDate), postgresFormat.parse(rs.getString(5)), reportItem);
                    endC = Calendar.getInstance();
                    endC.setTime(postgresFormat.parse(rs.getString(5)));
                    endC.set(5, endC.getActualMaximum(5));
                    if (reportItem != null) {
                        reportItem.addPerformance(new ItemPerformance(report_dateFormat.format(postgresFormat.parse(rs.getString(5))), report_dateFormat.format(endC.getTime()), rs.getString(6), rs.getString(7)));
                    }

                    endC.setTime(postgresFormat.parse(rs.getString(5)));
                    endC.add(2, 1);
                }

                if (reportItem != null) {
                    this.fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
                    reportItems.add(reportItem);
                }
            }

            rs.close();
            st.close();
            connection.close();
        } catch (Exception var25) {
            this.log.error("Repository Report failed: ", var25);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }

    }

    public void executeBatchItems(List<ReportItem> reportItems, String repositoryIdentifier, String itemDataType, Date beginDate, Date endDate, String granularity) {
        SimpleDateFormat report_dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat postgresFormat = new SimpleDateFormat("yyyy/MM");
        String beginDateStr = postgresFormat.format(beginDate);
        String endDateStr = postgresFormat.format(endDate);
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            connection = this.usageStatsDB.getConnection();
            if (itemDataType.equals("")) {
                st = connection.prepareStatement("SELECT distinct res.result_id, r.title, r.publisher, r.source, rc.type, pids.pid, d.name, res.`date`, oids.oid, res.downloads, res.views FROM (SELECT us.repository_id, us.result_id, us.`date`, sum(us.downloads) as downloads, sum(us.views) as views FROM " + this.usagestatsImpalaDB + ".usage_stats us WHERE us.`date`>=? AND us.`date`<=? AND us.repository_id=? GROUP BY us.repository_id, us.result_id, us.`date`) AS res JOIN " + this.statsDB + ".result r ON res.result_id=r.id JOIN " + this.statsDB + ".datasource d ON d.id=res.repository_id JOIN " + this.statsDB + ".result_classifications rc ON rc.id=r.id LEFT JOIN (SELECT pids.id, group_concat(pids.pid, '#!#') AS pid FROM " + this.statsDB + ".result_pids pids, " + this.statsDB + ".result_datasources rd WHERE rd.id=pids.id AND type='Digital Object Identifier' AND rd.datasource=? GROUP BY pids.id) AS pids ON pids.id=r.id LEFT JOIN (SELECT oids.id, group_concat(oids.oid, '#!#') AS oid FROM " + this.statsDB + ".result_oids oids, " + this.statsDB + ".result_datasources rd WHERE rd.id=oids.id AND rd.datasource=? GROUP BY oids.id) AS oids ON oids.id=r.id ORDER BY res.result_id, res.`date`;");
                st.setString(1, beginDateStr);
                st.setString(2, endDateStr);
                st.setString(3, repositoryIdentifier);
                st.setString(4, repositoryIdentifier);
                st.setString(5, repositoryIdentifier);
            } else {
                st = connection.prepareStatement("SELECT distinct res.result_id, r.title, r.publisher, r.source, rc.type, pids.pid, d.name, res.`date`, oids.oid, res.downloads, res.views FROM (SELECT us.repository_id, us.result_id, us.`date`, sum(us.downloads) as downloads, sum(us.views) as views FROM " + this.usagestatsImpalaDB + ".usage_stats us WHERE us.`date`>=? AND us.`date`<=? AND us.repository_id=? GROUP BY us.repository_id, us.result_id, us.`date`) AS res JOIN " + this.statsDB + ".result r ON res.result_id=r.id JOIN " + this.statsDB + ".datasource d ON d.id=res.repository_id JOIN " + this.statsDB + ".result_classifications rc ON rc.id=r.id LEFT JOIN (SELECT pids.id, group_concat(pids.pid, '#!#') AS pid FROM " + this.statsDB + ".result_pids pids, " + this.statsDB + ".result_datasources rd WHERE rd.id=pids.id AND type='Digital Object Identifier' AND rd.datasource=? GROUP BY pids.id) AS pids ON pids.id=r.id LEFT JOIN (SELECT oids.id, group_concat(oids.oid, '#!#') AS oid FROM " + this.statsDB + ".result_oids oids, " + this.statsDB + ".result_datasources rd WHERE rd.id=oids.id AND rd.datasource=? GROUP BY oids.id) AS oids ON oids.id=r.id WHERE rc.type=? ORDER BY res.result_id, res.`date`;");
                st.setString(1, beginDateStr);
                st.setString(2, endDateStr);
                st.setString(3, repositoryIdentifier);
                st.setString(4, repositoryIdentifier);
                st.setString(5, repositoryIdentifier);
                st.setString(6, itemDataType);
            }

            rs = st.executeQuery();
            String result = "";
            String lastDate = "";
            ReportItem reportItem = null;
            int ft_total = 0;
            int abstr = 0;
            if (!granularity.equalsIgnoreCase("totals")) {
                if (granularity.equalsIgnoreCase("monthly")) {
                    Calendar endCal = Calendar.getInstance();
                    endCal.setTime(postgresFormat.parse(endDateStr));
                    endCal.add(2, 1);

                    Date endDateForZeros;
                    Calendar endC;
                    for(endDateForZeros = endCal.getTime(); rs.next(); lastDate = postgresFormat.format(endC.getTime())) {
                        if (!rs.getString(1).equals(result)) {
                            if (reportItem != null) {
                                this.fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
                                reportItems.add(reportItem);
                            }

                            result = rs.getString(1);
                            lastDate = beginDateStr;
                            reportItem = new ReportItem(rs.getString(3), rs.getString(7), rs.getString(5), rs.getString(2));
                            reportItem.addIdentifier(new ItemIdentifier("OpenAIRE", rs.getString(1)));
                            reportItem.addIdentifier(new ItemIdentifier("URLs", rs.getString(4)));
                            if (rs.getString(9) != null && !rs.getString(9).equals("")) {
                                if (rs.getString(9).contains("#!#")) {
                                    reportItem.addIdentifier(new ItemIdentifier("OAI", rs.getString(9).substring(0, rs.getString(9).indexOf("#!#"))));
                                } else {
                                    reportItem.addIdentifier(new ItemIdentifier("OAI", rs.getString(9)));
                                }
                            }

                            if (rs.getString(6) != null && !rs.getString(6).equals("")) {
                                if (rs.getString(6).contains("#!#")) {
                                    reportItem.addIdentifier(new ItemIdentifier("DOI", rs.getString(6).substring(0, rs.getString(6).indexOf("#!#"))));
                                } else {
                                    reportItem.addIdentifier(new ItemIdentifier("DOI", rs.getString(6)));
                                }
                            }
                        }

                        this.fillWithZeros(postgresFormat.parse(lastDate), postgresFormat.parse(rs.getString(8)), reportItem);
                        endC = Calendar.getInstance();
                        endC.setTime(postgresFormat.parse(rs.getString(8)));
                        endC.set(5, endC.getActualMaximum(5));
                        if (reportItem != null) {
                            reportItem.addPerformance(new ItemPerformance(report_dateFormat.format(postgresFormat.parse(rs.getString(8))), report_dateFormat.format(endC.getTime()), rs.getString(10), rs.getString(11)));
                        }

                        endC.setTime(postgresFormat.parse(rs.getString(8)));
                        endC.add(2, 1);
                    }

                    if (reportItem != null) {
                        this.fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
                        reportItems.add(reportItem);
                    }
                }
            } else {
                while(rs.next()) {
                    if (!rs.getString(1).equals(result)) {
                        if (reportItem != null) {
                            reportItem.addPerformance(new ItemPerformance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate), Integer.toString(ft_total), Integer.toString(abstr)));
                            reportItems.add(reportItem);
                        }

                        result = rs.getString(1);
                        reportItem = new ReportItem(rs.getString(3), rs.getString(7), rs.getString(5), rs.getString(2));
                        reportItem.addIdentifier(new ItemIdentifier("OpenAIRE", rs.getString(1)));
                        reportItem.addIdentifier(new ItemIdentifier("URLs", rs.getString(4)));
                        if (rs.getString(9) != null && !rs.getString(9).equals("")) {
                            if (rs.getString(9).contains("#!#")) {
                                reportItem.addIdentifier(new ItemIdentifier("OAI", rs.getString(9).substring(0, rs.getString(9).indexOf("#!#"))));
                            } else {
                                reportItem.addIdentifier(new ItemIdentifier("OAI", rs.getString(9)));
                            }
                        }

                        if (rs.getString(6) != null && !rs.getString(6).equals("")) {
                            if (rs.getString(6).contains("#!#")) {
                                reportItem.addIdentifier(new ItemIdentifier("DOI", rs.getString(6).substring(0, rs.getString(6).indexOf("#!#"))));
                            } else {
                                reportItem.addIdentifier(new ItemIdentifier("DOI", rs.getString(6)));
                            }
                        }

                        ft_total = 0;
                        abstr = 0;
                    }

                    ft_total += rs.getInt(10);
                    abstr += rs.getInt(11);
                }

                if (reportItem != null) {
                    reportItem.addPerformance(new ItemPerformance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate), Integer.toString(ft_total), Integer.toString(abstr)));
                    reportItems.add(reportItem);
                }
            }
        } catch (Exception var25) {
            this.log.error("Batch Item Report failed: ", var25);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }

    }

    private void fillWithZeros(Date from, Date to, ReportItem reportItem) {
        SimpleDateFormat report_dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar fromCalendar = Calendar.getInstance();
        fromCalendar.setTime(from);
        Calendar toCalendar = Calendar.getInstance();
        toCalendar.setTime(to);

        while(from.before(to)) {
            Calendar temp_c = Calendar.getInstance();
            temp_c.setTime(from);
            temp_c.set(5, temp_c.getActualMaximum(5));
            Date temp_endDate = temp_c.getTime();
            reportItem.addPerformance(new ItemPerformance(report_dateFormat.format(from), report_dateFormat.format(temp_endDate), "0", "0"));
            fromCalendar.add(2, 1);
            from = fromCalendar.getTime();
        }

    }
}